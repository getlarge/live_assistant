const mqtt = require('mqtt');
const fetch = require('node-fetch');
const fs = require('fs');

class Connector {
    constructor(config) {
        this.deviceName = config.name;
        this.config = config;

        if(config.name.match(/[/ #+]/g)) {
            return console.log('Error');
        }

        this.mqttConfig = {
            username : config.name, 
            password : config.aloes.token,
            resubscribe : false,
            reconnectPeriod : 60000
        };
        

        this.initMqttClient();
        this.ready = false;
        //this.prefix = 
    }

    initMqttClient() {

        console.log('Aloes Config ->', this.config.aloes); 

        this.mqttClient = mqtt.connect(this.config.aloes.brokerUrl, this.mqttConfig);
        
        this.mqttClient.on('connect',  () => {
            this.ready = true;
           //this.mqttClient.subscribe('presence');
           //this.mqttClient.publish('presence', 'Hello mqtt');
        });
    }

    fetch(url, method, body) {
        let requestOptions = {method : method, headers : { 'x-access-token' : this.config.aloes.token, 'Content-Type': 'application/json' }};

        if(body) {
            requestOptions.body = JSON.stringify(body);
        }

        return fetch(this.config.aloes.apiUrl+url, requestOptions)
                    .then(res => res.json())
                    .catch((e) => {
                        console.error('Error', e);
                    });
    }

    publish(topic, message) {
        this.mqttClient.publish('/device/'+this.deviceName+'/'+topic, message);
    }
}

class Device extends Connector {

    constructor(config, afterLaunch) {
        super(config);
        this.config = config;
        this.sensorsTableAddress = {};
        this.sensors = {};
        this.listeners = {};

        this.mqttClient.on('connect',  () => {
            this.init()
                .then(() => {
                    afterLaunch(this);
                })
                .catch(console.error);
        });

        this.mqttClient.on('message', (topic, message) => {
            let sensorAddress = topic.split('/')[2];
            let event = topic.split('/')[3];
            try {
                this.listeners[sensorAddress][event](event, message.toString());
            } catch(e) {
                console.log('no listener for sensor : ', sensorAddress, e);
            }            
        });
    }

    subscribeSensor(sensor) {
        if(!this.listeners[sensor.address]) {
            this.listeners[sensor.address] = {};
        }

        this.sensorsTableAddress[sensor.externalId] = sensor.address;
    }

    init() {
        return new Promise((resolve, reject) => {
            this.fetch('/sensor', 'GET')
                .then(res => {

                    //console.log(res);
                    if(!res) {
                        return reject('error');
                    }

                    if(res.status > 400) {
                        return reject(res.error);
                    }

                    for(var i = 0; i < res.sensors.length; i++) {
                        let newSensor = new Sensor(this, res.sensors[i]);
                        this.subscribeSensor(newSensor);
                    }

                    resolve(res.sensors);
                });
            });
    }

    addSensor(info) {
        return new Promise((resolve, reject) => {
            let newSensor = new Sensor(this, {externalId: info.id, measurement : info.measurement});
            this.sensors[info.id] = newSensor;
            this.subscribe(newSensor)
                .then(() => {
                    resolve(newSensor);
                });
        });
    }

    publish(topic, message) {
        if(typeof topic !== "string") {
            throw 'topic should be a string';
        }

        if(topic[0] === '/') {
            //Authorise /path
            topic = topic.slice(1, topic.length);
        }

        if(typeof message !== "string") {
            message = JSON.stringify(message);
        }

        this.mqttClient.publish('/device/'+this.config.name+'/'+topic, message);        
    }

    subscribe(sensor) {
        return new Promise((resolve, reject) => {
            if(!this.sensorsTableAddress[sensor.externalId]) {
                this.fetch('/sensor/new', 'POST', { externalId : sensor.externalId, measurement : sensor.measurement})
                    .then((res) => {
                        if(res.status > 400) {
                            return console.log(res);
                        }

                        //console.log(res.sensors);
                        let newSensor = new Sensor(this, res);
                        this.subscribeSensor(newSensor);
                        resolve(res);
                    });
            } else {
                resolve(sensor);
            }
        });
    }

    publishSensor(sensor, message) {
        let formattedValue = {value : message};
        if(typeof message === 'object') {
            formattedValue = message;
        }

        if(this.sensorsTableAddress[sensor.externalId]) {
            let formattedMessage = Object.assign({ time : new Date() }, formattedValue);
            this.mqttClient.publish('/sensor/'+this.sensorsTableAddress[sensor.externalId]+'/data', JSON.stringify(formattedMessage));
        }
    }

    addListener(sensor, event, callback) {
        if(!this.listeners[this.sensorsTableAddress[sensor.externalId]][event]) {
            this.listeners[this.sensorsTableAddress[sensor.externalId]][event] = callback;
            console.log('Subscribe to : ', '/sensor/'+this.sensorsTableAddress[sensor.externalId]+'/'+event);
            this.mqttClient.subscribe('/sensor/'+this.sensorsTableAddress[sensor.externalId]+'/'+event);
        }   
    }
}

class Sensor {
    constructor(device, options) {
        this.device = device;
        this.externalId = options.externalId;
        this.measurement = options.measurement;
        
        if(options.address) {
            this.address = options.address;
        }
    }

    pub(data) {
        this.device.publishSensor(this, data);
    }

    on(event, callback) {
        this.device.addListener(this, event, callback.bind(this));
    }

    fetch() {

    }
}

module.exports = Device;
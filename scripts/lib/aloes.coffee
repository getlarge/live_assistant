Device = require('../connector')

aloes = {}
device = {}

aloes.createDevice = (botName, robot) -> 
  device = new Device({
    name: botName,
    plugins : {
      hubot : robot
    },
    aloes:
      brokerUrl: "mqtt://192.168.1.97:1885",
      apiUrl: 'http://192.168.1.97:8081',
      token: 'eyJhbGciOiJIUzI1NiJ9.eyJhbG9lc0FjY291bnRJZCI6IjViNTFkZTU5LTNmZTAtNGVkZC04NWE5LTdhMjA5NjZhNWJlMiIsImFsb2VzRWxlbUlkIjoiNDYxOThhOGEtMjUzNy00NjgwLTlmYmYtN2YzY2RmMDY2MTNiIiwiYWxvZXNFbGVtVHlwZSI6ImRldmljZSIsImFsb2VzRWxlbUluZm9zIjp7Im5hbWUiOiJteS1jYW0iLCJ0eXBlIjoiY2FtIn19.mjvEiZFrxuJXMTbXlVRjYhDqBnResMmyZytTqsSkY2s'
  }, () ->  )
  return device

aloes.getDevice = () ->
  return device

aloes.createSensor = (userId, channel, robot) -> 
  sensor = {}
  device.addSensor({
    id : userId,
    measurement : channel
  }).then((sensor)-> 
    sensor.on 'events', (event, message) ->
      try
        message = JSON.parse message
      catch e
        console.log " not a JSON file ", message, e
        return
        
      newMsg = {
        channel: message.channel,
        msg: message.topic,
        attachments: [{
            text: message.payload
        }]
      }
      console.log 'aloes sensor event -> ', message
      robot.adapter.chatdriver.customMessage(newMsg)

    sensor.pub = (payload, action) ->
      msg: payload,
      action: action
    return 
    )
  return sensor

module.exports = aloes


const mqtt = require('mqtt');
const AsyncClient = require('async-mqtt').AsyncClient;
const EventBus = require('../bot/event')

const clientUrl = process.env.MQTT_CLIENT_URL || "mqtt://localhost:1884";
const clientHost = process.env.MQTT_CLIENT_HOST || "localhost";
const clientPort = Number(process.env.MQTT_CLIENT_PORT) || 1884;
const clientId = process.env.MQTT_CLIENT_ID || "portal-mysensors";
const user =  process.env.MQTT_CLIENT_USER || "";
const pass = process.env.MQTT_CLIENT_PASS || "";

var options = {
    host: clientHost,
    port: clientPort,
  	clientId: clientId + "_" + Math.random().toString(16).substr(2, 8),
  	username: user,
  	password: new Buffer(pass)
};

class mqttClient  {
  constructor() {
    //super();
    this.client; 
    this._initClient();
    this.asyncClient;
    this.buffer = [];
    this.frame = {};
    this.subscribeList = [];
    this.ready = false;
  }

  _initClient() {
    this.client = mqtt.connect(clientUrl, options);
    EventBus.on('mqtt-sub', (topic) => {
      this.sub(topic)
    });
    EventBus.on('mqtt-unsub', (topic) => {
      this.unsub(topic)
    });
    EventBus.on('mqtt-tx', (topic, message) => {
      this.sendMessage(topic, message)
    });

  }

  _initAsyncClient(client) {
    this.asyncClient = new AsyncClient(client);
  }

  openStream() {
    this.client.on("connect", () => {
      this.ready = true;
      this._initAsyncClient(this.client)
      //this.synth.synthQuick("C4")
      this.setStatus("Connected");
    })
        
    this.client.on("disconnect", () => {
      this.setStatus("Disconnected");
    })

    this.client.on("end", () => {
      this.setStatus("Ended");
    })

    this.client.on("message", (topic, payload) => {
      var newPayload = topic + ">" + payload.toString();
      EventBus.emit("mqtt-rx", (topic, payload));            
      var topicSplit = topic.split("/");
      if (topicSplit[6] == "37" ) {
        //this.formatIncomingMessage("json", newPayload, "got-sound-frame");
      }
      if (topicSplit[6] == "38" ) {
        //this.formatIncomingMessage("json", newPayload, "got-volt-frame");
      }
    })
  }

  formatIncomingMessage(type, message, event) {
    var messageSplit = message.split(">");
    if (type = "json") {
      var payloadSplit = messageSplit[1].toString().split('/');
      /// formater le timestamp en format prêt à afficher sur D3
      // var day = moment(Number(payloadSplit[1]));
      // console.log("date", day)
      var x = Number(payloadSplit[1]);
      var y = Number(payloadSplit[0]);
      var topicSplit = messageSplit[1].split('/');
      var sensorType = topicSplit[6];
      var formatedPayload = {
        x: x,
        y: y,
        type: sensorType
      }; 
      this.buffer.push(formatedPayload);
    }
    if (this.buffer.length == 100 ) {
      this.buffer.pop();
    }
  }

  close() {
    this.client.end();
    return this.asyncClient.end();
  }

  setStatus(status) {
    //console.log("mqtt status:" , status)
    EventBus.emit("got-status", status);
  }

  sendAsyncMessage(topic, message) {
    this.asyncClient.publish(topic, message, { retain: false, qos: 0 })
      .then(function(){
        console.log("send message :", message, "to", topic )
        return this.asyncClient.end();
    });
  }

  sendMessage(topic, message) {
    this.client.publish(topic, message, { retain: false, qos: 0 });
  }

  sub(topic) {
    this.client.subscribe(topic);
      //console.log("subscribed to :", topic)   
  }

  unsub(topic) {
    this.client.unsubscribe(topic);
  }

  addSubscribe(path, topic) {
    this.asyncClient.subscribe(topic).then(function(){
      //console.log("subscribed to :", topic)
    });

    var pathList = path + "/" + topic;
    this.subscribeList.push(pathList);
    if (this.subscribeList.length == 100 ) {
      this.subscribeList.pop();
    }    
  }

  removeSubscribe(path, topic) {
    this.asyncClient.unsubscribe(topic).then(function(){
      //console.log("unsubscribed from :", topic)
    });
    var pathList = path + "/" + topic;
    console.log("index subscribe list :", this.subscribeList.indexOf(pathList));
  }

}


module.exports = mqttClient;

path = require 'path'
natural = require 'natural'
EventBus = require('../bot/event')

{ createDevice, getDevice, createSensor } = require path.join '..', 'lib','aloes.coffee'
{ msgVariables, stringElseRandomKey } = require path.join '..', 'lib', 'common.coffee'

answers = {}
livechat_department = (process.env.LIVECHAT_DEPARTMENT_ID || null )

class respond
  constructor: (@interaction) ->
  process: (msg) =>
    lc_dept = @interaction.department or livechat_department
    offline_message = @interaction.offline or 'Sorry, there is no online agents to transfer to.'
    type = @interaction.type?.toLowerCase() or 'random'
    switch type
      when 'block'
        messages = @interaction.answer.map (line) ->
          return msgVariables line, msg
        msg.sendWithNaturalDelay messages
      when 'random'
        message = stringElseRandomKey @interaction.answer
        message = msgVariables message, msg
        msg.sendWithNaturalDelay message

    command = @interaction.command?.toLowerCase() or false
    topic = msg.robot.name + "/" + command

    switch command
      when 'transfer'
        @livechatTransfer(msg, 3000, lc_dept, offline_message, type)
      when 'endchat'
        @endChat()
      when 'sendstate'
        payload = msg.envelope.message.text
        # @sendAloesEvents(action, payload, 3000)
        @sendUserEvents(topic, payload)
      when 'waitstream'
        wait = @interaction.delay
        @receiveAloesEvents(msg, wait)


  livechatTransfer: (msg, delay=3000, lc_dept, offline_message, type) ->
    setTimeout((-> msg.robot.adapter.callMethod('livechat:transfer',
                      roomId: msg.envelope.room
                      departmentId: lc_dept
                    ).then (result) ->
                      if result == true
                        console.log 'livechatTransfer executed!'
                      else
                        console.log 'livechatTransfer NOT executed!'
                        switch type
                          when 'block'
                            messages = offline_message.map (line) ->
                              return msgVariables line, msg
                            msg.sendWithNaturalDelay messages
                          when 'random'
                            message = stringElseRandomKey offline_message
                            message = msgVariables message, msg
                            msg.sendWithNaturalDelay message
                ), delay)

  endChat: () ->
    console.log 'transmit bot event ->', 'fin du chat'
    return

  sendUserEvents: (topic, payload) ->
    EventBus.emit("mqtt-tx", topic, payload);
    console.log 'transmit bot event ->', topic, payload
    return

  sendAloesEvents: (action, payload, delay = 3000) ->
    aloesBot = getDevice()
    console.log 'transmit bot event ->', payload, action
    aloesBot.sensors.ed.pub
      msg: payload
      action: action
    return 

module.exports = respond


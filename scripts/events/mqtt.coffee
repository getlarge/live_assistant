path = require 'path'
natural = require 'natural'

EventBus = require path.join '..', 'bot', 'event.coffee'
{ msgVariables, sendMessages, stringElseRandomKey } = require path.join '..', 'lib', 'common.coffee'

answers = {}

class mqtt
	constructor: (@interaction) ->
	process: (msg) =>
		offline_message = @interaction.offline or 'Sorry, there is no online agents to transfer to.'
		type = @interaction.type?.toLowerCase() or 'random'
		switch type
      when 'block'
        messages = @interaction.answer.map (line) ->
          return msgVariables line, msg
        msg.sendWithNaturalDelay messages
      when 'random'
        message = stringElseRandomKey @interaction.answer
        message = msgVariables message, msg
        msg.sendWithNaturalDelay message

    command = @interaction.command?.toLowerCase() or false
		pub = msg.message.text.split(':')

		switch command
			when "subscribe"
				topic = pub[1]
				@subscribing(msg, topic)
			when "unsubscribe"
				topic = pub[1]
				@unsubscribing(msg, topic)            
			when "publish"
				topic = pub[1]
				payload = pub[2]
				@publishing(msg, topic, payload)
			when "mail"
				topic = @interaction.topic
				payload = pub[1]
				@publishing(msg, topic, payload)

	subscribing: (msg, topic) ->
		EventBus.emit "mqtt-sub", topic
		console.log 'mqtt subscribe ->', topic

	unsubscribing: (msg, topic) ->
		console.log 'mqtt unsubscribe ->', topic
		EventBus.emit "mqtt-unsub", topic

	publishing: (msg, topic, payload) ->
		console.log 'mqtt publish ->', topic, payload
		EventBus.emit "mqtt-tx", topic, payload

module.exports = mqtt

